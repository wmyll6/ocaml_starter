(* OASIS_START *)
(* DO NOT EDIT (digest: 0c2ed76b4a19285829dcd928978f582e) *)

fpcontest2015march - Solve problem from
http://haskell98.blogspot.co.uk/2015/03/2015.html
=========================================================================================

See the file [INSTALL.txt](INSTALL.txt) for building and installation
instructions.

Copyright and license
---------------------

fpcontest2015march is distributed under the terms of the MIT License.

(* OASIS_STOP *)
