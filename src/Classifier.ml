type amount_to_countries = string * string list

type bug_description = {
  bug_name: string;
  amount: amount_to_countries list
}

exception Invalid_file_format of string

let parse_dat_file content =

  (* Parse line where amount criterion and comma-separated countries are specified 'c: c1,c2,c3'*)
  let parse_amount line = match Str.bounded_split (Str.regexp ":") line 2 with
    | [] -> raise (Invalid_file_format line)
    | cr::countries ->
      let cr = String.trim cr
      and countries = (List.map String.trim (Str.split_delim (Str.regexp ",") (List.hd countries)))
      in (cr, countries)

  and is_not_empty_line line =
    (String.length (String.trim line)) <> 0

  and content = String.trim content in

  let lines = Str.split_delim (Str.regexp "\n") (content) in
  match lines with
  | [] -> raise (Invalid_file_format content)
  | name::amount_lines -> {
      bug_name = name;
      amount = List.map parse_amount (List.filter is_not_empty_line amount_lines)
    }
