open Classifier

(* By default when test case fails we don't see the reason as 'assert_equal'
function doesn't know how to show instance of compared objects.
And of course doesn't know how to correctly. We need to implement such diff
function ourself

   Look through different pp_diff implementations on github
   https://github.com/search?p=3&q=pp_diff+filename%3A%2A.ml&ref=searchresults&type=Code&utf8=%E2%9C%93
*)
let pp_diff_str fmt (expected, actual) =
  if expected <> actual then
    Format.fprintf fmt "'%s' <> '%s'\n" expected actual

let bug_description_to_string { bug_name = n; amount = a} =
  let amount_to_string (c, countries) =
    Format.sprintf "'%s', [%s]" c (String.concat "," (List.map (fun s -> Format.sprintf "'%s'" s) countries)) in
  let amount_str = String.concat "," (List.map amount_to_string a) in
  Format.sprintf "{ bug_name = '%s'; amount = [ %s ]}" n amount_str

let pp_diff_structure fmt (expected, actual) =
  if expected <> actual then
    (Format.fprintf fmt "%s <> %s\n"
       (bug_description_to_string expected)
       (bug_description_to_string actual))
