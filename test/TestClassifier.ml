open OUnit2;;
open Classifier;;

let test_dat_content_parsed ctx =
  let expected = {
      bug_name = "-bug-name";
      amount= [
         ("amount1", ["c1"; "c2"; "c3"]);
         ("amount3", ["c4"]);
         ("amount2", ["c6"; "c7"])]} in
  assert_equal ~pp_diff:Conftest.pp_diff_structure
    expected 
    (parse_dat_file "-bug-name

                    amount1: c1, c2, c3
                    amount3: c4
                    amount2: c6, c7")

let tests =
  "Test parse dat files" >:::
  [
    "Pasre dat file" >:: test_dat_content_parsed
  ]
